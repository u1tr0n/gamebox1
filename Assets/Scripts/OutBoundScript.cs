﻿using UnityEngine;

public class OutBoundScript : MonoBehaviour
{
    /// <summary>
    /// Если игрок вылетел за пределы карты - нужно телепортировать его на цнтр карты
    /// </summary>
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.position = new Vector3(0f, 0f, 1.5f);
        }
    }
}
