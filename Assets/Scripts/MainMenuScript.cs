﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    /// <summary>
    /// При нажатии кнопки "Играть" загружаем сцену с игрой
    /// </summary>
    public void StartClick()
    {
        SceneManager.LoadScene(1);
    }
}
