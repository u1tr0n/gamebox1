﻿using System.Collections;
using Assets.Scripts;
using UnityEngine;

public class BonusScript : MonoBehaviour
{
    /// <summary>
    /// Ссылка на гейм менеджера
    /// </summary>
    public GameManager _gm;
    /// <summary>
    /// Ссылка на эффект поедания бонуса
    /// </summary>
    public ParticleSystem _eat_effect;
    /// <summary>
    /// Ссылка на модель бонуса
    /// </summary>
    public GameObject _body;
    /// <summary>
    /// Кол-во очков которые добавляем бонус
    /// </summary>
    public int addScore = 0;
    /// <summary>
    /// Кол-во жизней которое добавляет бонус
    /// </summary>
    public int addLifes = 0;
    /// <summary>
    /// Кол-во силы, которое добавляет бонус
    /// </summary>
    public float addStr = 0;

    /// <summary>
    /// Шанс на появление этого бонуса в зависимости от счета игрока деленного на 100
    /// </summary>
    public AnimationCurve chanceToSpawn;

    
    /// <summary>
    /// Кол-во массы, которое добавит бонус
    /// </summary>
    public float addMass;

    /// <summary>
    /// Минимальное время уничтожения бонуса
    /// </summary>
    public float minExpireTime;
    /// <summary>
    /// Максимальное время уничтожения бонуса
    /// </summary>
    public float maxExpireTime;

    /// <summary>
    /// Минимальное скорость вращения бонуса
    /// </summary>
    public float minRotationSpeed;
    /// <summary>
    /// Максимальное скорость вращения бонуса
    /// </summary>
    public float maxRotationSpeed;
    
    /// <summary>
    /// текщая скорость вращения бонуса
    /// </summary>
    private float rotation_speed;

    void Start()
    {
        // Если максимальное время уничтожения бонуса больше нуля 
        if (maxExpireTime > 0)
        {
            // Запускаем корутину уничтожения бонуса
            StartCoroutine(ExpireCoroutine());
        }
        // генерируем скорость вращения бонуса
        rotation_speed = Random.Range(minRotationSpeed, maxRotationSpeed);
    }
    
    IEnumerator ExpireCoroutine()
    {
        // Генерирум время жизни бонуса 
        float timeExpire = Random.Range(minExpireTime, maxExpireTime);
        //Debug.Log($"spawn bonus for {timeExpire} seconds");
        // Ждем указанное время
        yield return new WaitForSeconds(timeExpire);
        //Debug.Log("Remove bonus");
        //уничтожаем бонус
        Destroy(gameObject);
    }

    void FixedUpdate()
    {
        // Крутим бонус
        transform.Rotate(Vector3.up, rotation_speed * Time.fixedDeltaTime);
    }

    private IEnumerator RemoveBonus()
    {
        //уничтожаем бонус после проигрывания анимации поедания
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Eated bonus trigger");
        if (other.CompareTag("Player"))
        {
            //Debug.Log("Confirm eated by player");
            if (_eat_effect != null)
            {
                transform.Rotate(new Vector3(0-transform.rotation.x, 0-transform.rotation.y,0-transform.rotation.x));
                _eat_effect.Play(true);
            }

            if (_body)
            {
                _body.SetActive(false);
            }
            _gm.addScore(addScore);
            _gm.AddLifes(addLifes);
            PlayerScript ps = other.GetComponent<PlayerScript>();
            if (ps != null)
            {
                ps.AddMass(addMass);
                ps.AddStr(addStr);
            }

            StartCoroutine(RemoveBonus());
        }
        else
        {
            //Debug.Log("Eated by: " + other.tag);
        }
    }
}
