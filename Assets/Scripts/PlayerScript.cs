﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    public class PlayerScript : MonoBehaviour
    {
        /// <summary>
        /// Ткущий угол поворота утки (модели игрока)
        /// </summary>
        public int angel = 0;
        
        /// <summary>
        /// Константа гравитации
        /// </summary>
        public float gravity;

        /// <summary>
        /// Кол-во вертикальной энергии сообщаемое игроку при нажатии кнопки W
        /// </summary>
        public float upVerticalPower;
        /// <summary>
        /// Кол-во горизонтальной энергии сообщаемое игроку при нажатии кнопки A/D
        /// </summary>
        public float sideVerticalPower;
        /// <summary>
        /// Кол-во вертикальной энергии сообщаемое игроку при нажатии кнопки A/D
        /// </summary>
        public float sideHorizontalPower;

        /// <summary>
        /// Минимальный лимит вертикальной скорости игрока
        /// </summary>
        public float minVerticalSpeed;
        
        /// <summary>
        /// Максимальный лимит вертикальной скорости игрока
        /// </summary>
        public float maxVertivalSpeed;

        /// <summary>
        /// Текущая масса игрока
        /// </summary>
        public float duckMass;

        /// <summary>
        /// Сила утки
        /// </summary>
        public float duckStr;

        /// <summary>
        /// Кэш начальной массы игрока
        /// </summary>
        private float startDuckMass;
        

        /// <summary>
        /// Флаг была ли нажата кнопка W
        /// </summary>
        private bool _wPressed = false;
        /// <summary>
        /// Флаг была ли нажата кнопка A
        /// </summary>
        private bool _aPressed = false;
        /// <summary>
        /// Флаг была ли нажата кнопка D
        /// </summary>
        private bool _dPressed = false;
        /// <summary>
        /// Флаг была ли нажата кнопка S
        /// </summary>
        private bool _sPressed = false;
        
        /// <summary>
        /// Кэш ригибоди утки
        /// </summary>
        public Rigidbody _rb;
        /// <summary>
        /// Модель отображния утки
        /// </summary>
        public GameObject Mesh;
        /// <summary>
        /// Позиция игрока
        /// </summary>
        public Transform _Body;
        /// <summary>
        /// Ссылка на эффект умирания
        /// </summary>
        public ParticleSystem DieEffect;

        /// <summary>
        /// Ссылка на геймменеджер
        /// </summary>
        public GameManager _gm;

        /// <summary>
        /// Направление движения утки
        /// </summary>
        private int side = 3; // 1 - смотрит влево, 2 - смотрит в экран, 3 - смотрит вправо.

        public Vector3 move;
        /// <summary>
        /// Флаг Неуязвимости игрока (несколько секунд после отнимания жизни игрок неуязвим)
        /// </summary>
        private bool immortal = false;
        
        void Start()
        {
            /// Кэшируем стартовую массу утки
            startDuckMass = duckMass;
        }
        
        void Update()
        {
            // если нажаты кнопки управления выставляем соответствующие флаги
            if (!_wPressed)
            {
                _wPressed = Input.GetKeyDown(KeyCode.W);
            }
            if (!_aPressed)
            {
                _aPressed = Input.GetKeyDown(KeyCode.A);
            }
            if (!_dPressed)
            {
                _dPressed = Input.GetKeyDown(KeyCode.D);
            }
            if (!_sPressed)
            {
                _sPressed = Input.GetKeyDown(KeyCode.S);
            }
            // if(Input.GetKeyDown(KeyCode.Alpha1))
            // {
            //     Debug.Log("1 pressed");
            //     SceneManager.LoadScene(0);
            // }
        }

        /// <summary>
        /// Добавление массы игроку
        /// </summary>
        public void AddMass(float mass)
        {
            duckMass += mass;
            if (duckMass < startDuckMass)
            {
                duckMass = startDuckMass;
            }
        }
        /// <summary>
        /// Добавление силы игроку
        /// </summary>
        public void AddStr(float str)
        {
            duckStr += str;
        }

        /// <summary>
        /// Мгновенная компенсация гравитации
        /// </summary>
        private Vector3 FixGraviy()
        {
            return new Vector3(0,Mathf.Abs(_rb.velocity.y), 0);
        }

        private void FixedUpdate()
        {
            // тут обноляем физику
            move = Vector3.zero;
            Vector3 velocity = _rb.velocity;
                
            if (_wPressed)
            {
                velocity.y = 0;
                _rb.velocity = velocity;
                _rb.AddForce(Vector3.up * (upVerticalPower * duckStr), ForceMode.Impulse);
                _wPressed = false;
            }
            if (_aPressed)
            {
                velocity.y = 0;
                velocity.x = 0;
                _rb.velocity = velocity;
                _rb.AddForce(Vector3.up * (sideVerticalPower * duckStr) + FixGraviy(), ForceMode.Impulse);
                _rb.AddForce(Vector3.right * sideHorizontalPower, ForceMode.Impulse);
                _aPressed = false;
                // если меняем направление движения то поворачиваем модель игрока
                if (side != 1)
                {
                    _Body.Rotate(Vector3.up,  -180f);
                    Vector3 pos = Mesh.transform.position;
                    pos.x += .55f;
                    pos.z -= .65f;
                    Mesh.transform.position = pos;
                    side = 1;
                }
            }
            if (_sPressed)
            {
                velocity.x = 0;
                _rb.velocity = velocity;
                _sPressed = false;
            }
            if (_dPressed)
            {
                velocity.y = 0;
                velocity.x = 0;
                _rb.velocity = velocity;
                _rb.AddForce(Vector3.up * (sideVerticalPower * duckStr) + FixGraviy(), ForceMode.Impulse);
                _rb.AddForce(Vector3.left * sideHorizontalPower, ForceMode.Impulse);
                _dPressed = false;
                // если меняем направление движения то поворачиваем модель игрока
                if (side != 3)
                {
                    _Body.Rotate(Vector3.up,  180f);
                    Vector3 pos = Mesh.transform.position;
                    pos.x -= .55f;
                    pos.z += .65f;
                    Mesh.transform.position = pos;
                    side = 3;
                }
            }
            //Действуем гравитацией!
            velocity = new Vector3(_rb.velocity.x, Mathf.Clamp(_rb.velocity.y - gravity * duckMass, minVerticalSpeed, maxVertivalSpeed), _rb.velocity.z);
            _rb.velocity = velocity;
        }

        /// <summary>
        /// Корутина мигание игрока после забирания жизни 
        /// </summary>
        private IEnumerator PlayerStartBlink()
        {
            bool active = true;
            for (int i = 1; i <= 8; i++)
            {
                active = !active;
                Mesh.SetActive(active);
                yield return new WaitForSeconds(0.25f);
                
            }
            immortal = false;
            yield break;
        }

        /// <summary>
        /// Корутина рисующая смерть игрока и забирающая жизнь а если жизни закончились то показывает меню
        /// </summary>
        private IEnumerator Die()
        {
            if (_gm.TryDie())
            {
                Mesh.SetActive(false);

                DieEffect.Play();
                yield return new WaitForSeconds(0.5f);
                Destroy(gameObject);
                _gm.PlayerDie();
            }
            else
            {
                DieEffect.Play();
                duckMass -= duckMass / 2f;
                if (duckMass < startDuckMass)
                {
                    duckMass = startDuckMass;
                }
                yield return PlayerStartBlink();
            }
        }

        /// <summary>
        /// Обработка столкновения с барьерами
        /// </summary>
        private void OnTriggerEnter(Collider other)
        {
            //Debug.Log("Enter collision: " + other.tag);
            if (other.CompareTag("Barriers"))
            {
                if (!immortal)
                {
                    immortal = true;
                    StartCoroutine(Die());   
                }
            }
        }
    }
}
