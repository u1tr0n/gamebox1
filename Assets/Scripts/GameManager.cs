﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    /// <summary>
    /// Ссылка на элемет интерфейса отображения счета очков игрока 
    /// </summary>
    public TMP_Text scoreField;
    /// <summary>
    /// Ссылка на элемет интерфейса отображения Кол-ва жизней игрока 
    /// </summary>
    public TMP_Text lifesField;
    
    /// <summary>
    /// Ссылка на элемет интерфейса отображения меню рестарта после смерти 
    /// </summary>
    public GameObject dieScreen;

    /// <summary>
    /// Текущие показания счета игрока
    /// </summary>
    public int score = 0;
    /// <summary>
    /// Текущие показания Жизней игрока
    /// </summary>
    public int lifes = 0;

    /// <summary>
    /// Отображаемое кол-во очков игрока (нужно для анимации увеличения счета)
    /// </summary>
    public int displayedScore = 0;

    /// <summary>
    /// Задержка появления бонусов
    /// </summary>
    public float bonusIncrementDelay;

    /// <summary>
    /// Время последнего появления бонуса
    /// </summary>
    private float bonusIncrementLast;

    
    /// <summary>
    /// Х отступ игрового поля
    /// </summary>
    public float xOffset;
    
    /// <summary>
    /// У отступ игрового поля
    /// </summary>
    public float yOffset;
    
    /// <summary>
    /// Ширина рабочего поля
    /// </summary>
    public int spawnerWeight;
    /// <summary>
    /// Высота рабочего поля
    /// </summary>
    public int spawnerHeight;
    /// <summary>
    /// Начальная задержка появления бонуса
    /// </summary>
    public float spawnerStartDelay;
    /// <summary>
    /// Минимальная задержка появления бонуса
    /// </summary>
    public float spawnerMinDelay;
    /// <summary>
    /// Максимальная задержка появления бонуса
    /// </summary>
    public float spawnerMaxDelay;
    /// <summary>
    ///  текущее значение задержки появления игрока
    /// </summary>
    public float spawnerCurrentDelay;
    public float spawnerLastSpawn;
    public float spawnerSpawnSpeed;
    
    /// <summary>
    /// Ссылка на все бонусы на карте
    /// </summary>
    public GameObject[] bonuses;
    
    /// <summary>
    /// Это модель для теста хорошего места для появления бонуса (визализация только для дева)
    /// </summary>
    public GameObject spawnGoodObject;
    /// <summary>
    /// Это модель для теста плохого места для появления бонуса (визализация только для дева)
    /// </summary>
    public GameObject spawnBadObject;
    
    /// <summary>
    /// Массив возможных мест появления бонуса
    /// </summary>
    public int[,] acceptSpawn;
    /// <summary>
    /// Массив для отображения возможных позиций появления бонусов (только для дева)
    /// </summary>
    private GameObject[,] spawns;
    
    /// <summary>
    /// Флаг находится ли игра на паузе
    /// </summary>
    private bool isGamePauseed = false;
    
    /// <summary>
    /// Время последнего получения бонуса жизни
    /// </summary>
    private float lastLifeAdd;
    
    
    void Start()
    {
        spawnerCurrentDelay = spawnerStartDelay;
        fillAcceptSpawn();
        Screen.fullScreen = true;
        lifesField.text = lifes.ToString();
        //drawSpanCircles();
        //SaveData();
    }

    /// <summary>
    /// Заполнение массива возможных мест появления бонуса
    /// </summary>
    void fillAcceptSpawn()
    {
        acceptSpawn = new int[spawnerWeight,spawnerHeight];
        spawns = new GameObject[spawnerWeight,spawnerHeight];
        for (int w = 0; w < spawnerWeight; w++)
        {
            for (int h = 0; h < spawnerHeight; h++)
            {
                if (h < 3 || w < 3 || h > spawnerHeight - 3 || w > spawnerWeight - 3)
                {
                    acceptSpawn[w, h] = 0;
                }
                else
                {
                    if (w >= 7 && h >= 7 && w <= 20 && h <= 9)
                    {
                        acceptSpawn[w, h] = 0;
                    } else if (w >= 35 && h >= 7 && w <= 48 && h <= 9)
                    {
                        acceptSpawn[w, h] = 0;
                    } else if (w >= 26 && h >= 6 && w <= 29 && h <= 19)
                    {
                        acceptSpawn[w, h] = 0;
                    } else if (w >= 20 && h >= 20 && w <= 36 && h <= 22)
                    {
                        acceptSpawn[w, h] = 0;
                    } else if (w >= 7 && h >= 20 && w <= 16 && h <= 22)
                    {
                        acceptSpawn[w, h] = 0;
                    }  else if (w >= 7 && h >= 16 && w <= 8 && h <= 19)
                    {
                        acceptSpawn[w, h] = 0;
                    } else if (w >= 39 && h >= 20 && w <= 48 && h <= 22)
                    {
                        acceptSpawn[w, h] = 0;
                    } else if (w >= 47 && h >= 16 && w <= 48 && h <= 19)
                    {
                        acceptSpawn[w, h] = 0;
                    } else if (w >= 3 && h >= 29 && w <= 8 && h <= 30)
                    {
                        acceptSpawn[w, h] = 0;
                    } else if (w >= 12 && h >= 29 && w <= 23 && h <= 30)
                    {
                        acceptSpawn[w, h] = 0;
                    } else if (w >= 12 && h >= 31 && w <= 13 && h <= 39)
                    {
                        acceptSpawn[w, h] = 0;
                    } else if (w >= 32 && h >= 29 && w <= 43 && h <= 30)
                    {
                        acceptSpawn[w, h] = 0;
                    } else if (w >= 42 && h >= 31 && w <= 43 && h <= 39)
                    {
                        acceptSpawn[w, h] = 0;
                    } else if (w >= 47 && h >= 29 && w <= 52 && h <= 30)
                    {
                        acceptSpawn[w, h] = 0;
                    } else if (w >= 26 && h >= 37 && w <= 29 && h <= 42)
                    {
                        acceptSpawn[w, h] = 0;
                    }
                    else

                    {
                        acceptSpawn[w, h] = 1;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Отображение мест возможного появления бонуса
    /// </summary>
    void drawSpanCircles()
    {
        for (int w = 0; w < spawnerWeight; w++)
        {
            for (int h = 0; h < spawnerHeight; h++)
            {
                if (acceptSpawn[w, h] == 1)
                {
                    spawns[w, h] = Instantiate(spawnGoodObject, new Vector3(xOffset + w, yOffset - h, 3f), Quaternion.identity);
                }
                else
                {
                    spawns[w, h] = Instantiate(spawnBadObject, new Vector3(xOffset + w, yOffset - h, 3f), Quaternion.identity);
                }
            }
        }
    }
    
    /// <summary>
    /// Метод добавления жизни при подборе бонуса жизни
    /// </summary>
    public void AddLifes(int lifesAdd)
    {
        if (Time.time - lastLifeAdd > 0.1f)
        {
            lifes += lifesAdd;
            lifesField.text = lifes.ToString();
            lastLifeAdd = Time.time;
        }
    }
    
    /// <summary>
    /// Отчистить массив объектов отображения возможных мест появления бонусов
    /// </summary>
    void clearCircles()
    {
        for (int w = 0; w < spawnerWeight; w++)
        {
            for (int h = 0; h < spawnerHeight; h++)
            {
                Destroy(spawns[w, h]);
            }
        }
    }

    /// <summary>
    /// Выбор бонуса для появления согласно их шансам
    /// </summary>
    private int getBonusIndex()
    {
        // Сумма всех шансов всех бонусов
        float sum = 0f;
        // Список шансов каждого бонуса
        List<float> chances = new List<float>();
        // индекс бонуса в массиве
        int index = 0;
        // проходим по всем возможным бонусам
        foreach (var bn in bonuses)
        {
            BonusScript bs = bn.GetComponent<BonusScript>();
            //получаем их бону скрипты
            if (bs != null)
            {
                // получаем текущий шанс появления бонуса исходя из текущего счета игрока
                chances.Add(bs.chanceToSpawn.Evaluate(score/100f));
                // добавляем шанс к общей сумме шансов
                sum += chances[index];
                ++index;
            }
        }

        // Берем случайное число от нуля до суммы шанов появления бонусов
        float value = Random.Range(0f, sum);
        // проходим по списку всех шансов
        for (int i = 0; i <= index; i++)
        {
            // вычетаем из сгенерированного рандомного числа шанс появления текушего бонуса
            value -= chances[i];
            // если число стало отрицательным значит выпадает этот бонус
            if (value < 0)
            {
                return i;
            }
        }
        // что наврятли случится но если в никакой шанс не попали то озвращаем последний
        Debug.LogError("Shit happens spawn last");
        return index-1;
    }
    
    void Update()
    {
        //Проверяем не пора ли спаунить бонус
        if (spawnerLastSpawn + spawnerCurrentDelay < Time.time)
        {
            // Если пора добавлять бонус увеличиваем время последнего появления бонуса
            spawnerLastSpawn = Time.time;
            
            //увеличиваем задержку появления бонуса
            spawnerCurrentDelay =
                Mathf.Clamp(spawnerCurrentDelay + spawnerSpawnSpeed, spawnerMinDelay, spawnerMaxDelay);
            // Выбираем бонус для появления согласно шансам
            int bonusIndex = getBonusIndex(); //Random.Range(0, bonuses.Length);
            // рисуем бонус
            GameObject bonus = Instantiate(bonuses[bonusIndex], GenerateSpawnPoint(), Quaternion.identity);
            // даем бонусу ссылку на гейм менеджера
            bonus.GetComponent<BonusScript>()._gm = this;
        }

        // if(Input.GetKeyDown(KeyCode.Alpha2))
        // {
        //     Debug.Log("Clear!");
        //     //clearCircles();
        //     fillAcceptSpawn();
        //     //drawSpanCircles();
        // }
    }
    
    
    /// <summary>
    /// Рекурсионно проверям можем ли вы создать бонус в этой точке, если нет то пробуем еще раз пока не получится
    /// </summary>
    private Vector3 GenerateSpawnPoint()
    {
        int x = Random.Range(0, spawnerWeight);
        int y = Random.Range(0, spawnerHeight);
        if (acceptSpawn[x, y] == 1)
        {
            return new Vector3(xOffset + x, yOffset - y, 1f);
        }
        else
        {
            Debug.Log("GeneratorIteration");
            return GenerateSpawnPoint();
        }
    }

    /// <summary>
    /// Рисуем анимацию накрутки счета при необходимости
    /// </summary>
    void FixedUpdate()
    {
        if(displayedScore < score)
        {
            if (Time.time > bonusIncrementLast + bonusIncrementDelay)
            {
                displayedScore++;
                scoreField.text = displayedScore.ToString();
                bonusIncrementLast = Time.time;
            }
        }
    }

    /// <summary>
    /// Пробуем умиреть, если TRUE то смогли иначе забираем жизни и отрисовываем новое значение
    /// </summary>
    public bool TryDie()
    {
        if (--lifes > 0)
        {
            lifesField.text = this.lifes.ToString();
            return false;
        }
        else
        {
            return true;
        }
    }

    /// <summary>
    /// Метод умерчления игрока
    /// </summary>
    public void PlayerDie()
    {
        dieScreen.SetActive(true);
        Time.timeScale = 0;
        isGamePauseed = true;
    }

    /// <summary>
    /// Рестарт уровня
    /// </summary>
    public void restart()
    {
        Time.timeScale = 1;
        isGamePauseed = false;
        SceneManager.LoadScene(1);
    }

    /// <summary>
    /// Добавление счета
    /// </summary>
    public void addScore(int count)
    {
        score += count;
    }
}
