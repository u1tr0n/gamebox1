﻿using UnityEngine;

public class PortalScript : MonoBehaviour
{

    /// <summary>
    /// Объект на который телепортнется игрок при телепортировании
    /// </summary>
    public GameObject _portalTarget;
    /// <summary>
    /// Можель игрока клон которой будет показан как часть телепортируемого игрока
    /// </summary>
    public GameObject PlayerModel;

    /// <summary>
    /// Экземпляр можели игрока, расположенный на сцене
    /// </summary>
    private GameObject _playerModel;
    
    /// <summary>
    /// Позиция портала (это верхний телепорт или нижний, нужно для корректной работы телепорта)
    /// </summary>
    public PortalPosition _portal_position;
    /// <summary>
    /// Кеш экземпляра этого скрипта на целевом телепорте
    /// </summary>
    private PortalScript _portalScript;
    /// <summary>
    /// Отступы от края телепорта до точки телепорта
    /// </summary>
    private float offsetY = 1.5f;

    private void Start()
    {
        _portalScript = _portalTarget.GetComponent<PortalScript>();
    }

    private void OnTriggerEnter(Collider other)
    {
        /*
         * При входе еигрока в текущий телепорт показать клона в целевом телепорте
         */
        _portalScript.showModel(other.transform.position, transform.rotation);
    } 
    private void OnTriggerExit(Collider other)
    {
        /*
         * При выходе игрока из коллайдера (может произойти после телепорта или при отмене телепорта) уничтожать клона
         */
        _portalScript.removeModel();
    }
    /// <summary>
    /// Пока коллайдер работает нужно двигать клона подобно движению игрока
    /// </summary>
    private void OnTriggerStay(Collider other)
    {
        //считаем расстояние по Y от телепорта до игрока
        float val = transform.position.y - other.transform.position.y;
        if (_portal_position == PortalPosition.Top)
        {
            // Если это расстояние меньше константы то игрок полностью погрузился в модель телепорта и можно телепортировать
            if (val < -0.16f)
            {
                other.transform.position = _portalScript.getTeleportPosition();
            }
            else
            {
                // Если погрузился не полностью, то двигам клон подобно модели игрока
                _portalScript.moveModel(other.transform.position, val);
            }
        } else {
            // Если это расстояние больше константы то игрок полностью погрузился в модель телепорта и можно телепортировать
            if (val > -0.85f)
            {
                other.transform.position = _portalScript.getTeleportPosition();
            }
            else
            {
                // Если погрузился не полностью, то двигам клон подобно модели игрока
                _portalScript.moveModel(other.transform.position, transform.position.y - other.transform.position.y);
            }
        }
    }

    /// <summary>
    /// Метод отображения клона игрока на целевом портале
    /// </summary>
    public void showModel(Vector3 setPosition, Quaternion rotation)
    {
        Vector3 pos = setPosition;
        pos.y = transform.position.y - (_portal_position == PortalPosition.Bottom ? 0-offsetY : offsetY);
        _playerModel = Instantiate(PlayerModel, pos, transform.rotation, transform);
        _playerModel.SetActive(true);
    }

    /// <summary>
    /// Метод перемещения клона игрока на целевом портале
    /// </summary>
    public void moveModel(Vector3 newPosition, float offset)
    {
        Vector3 pos = newPosition;
        float offset_value = _portal_position == PortalPosition.Bottom ? offset - offsetY : offset + offsetY;
        pos.y = transform.position.y - offset_value;
        _playerModel.transform.position = pos;
    }

    /// <summary>
    /// Удалить клон игрока на целевом портале
    /// </summary>
    public void removeModel()
    {
        Destroy(_playerModel);
    }

    /// <summary>
    /// Получить координаты клона игрока чтоб замнить его игроком при телепорте
    /// </summary>
    public Vector3 getTeleportPosition()
    {
        return _playerModel.transform.position;
    }
}

/// <summary>
/// Позиция портала (телепорты бывают верхние и нижние)
/// </summary>
public enum PortalPosition
{
    Top,
    Bottom,
}
